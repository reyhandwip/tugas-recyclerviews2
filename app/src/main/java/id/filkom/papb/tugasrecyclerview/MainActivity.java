package id.filkom.papb.tugasrecyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static String TAG = "RV1";
    RecyclerView rv1;
    Button btSimpan;
    EditText etNama, etNama2;
    MahasiswaAdapter mahasiswaAdapter;
    ArrayList<Mahasiswa> mahasiswaList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        rv1.setHasFixedSize(true);

        btSimpan = findViewById(R.id.btSimpan);
        etNama = findViewById(R.id.etNama);
        etNama2 = findViewById(R.id.etNama2);


        btSimpan.setOnClickListener(v ->{
            mahasiswaList.add(new Mahasiswa(etNama.getText().toString(), etNama2.getText().toString()));
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        mahasiswaList.add(new Mahasiswa("215150400111062", "Reyhan Dwi Prasetyo"));
        mahasiswaList.add(new Mahasiswa("215150400111023", "Michael Rahadian Wijaya"));
        mahasiswaAdapter = new MahasiswaAdapter(this, mahasiswaList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        rv1.setLayoutManager(layoutManager);

        mahasiswaAdapter.setOnItemClickistener((position, v) -> {
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        rv1.setAdapter(mahasiswaAdapter);


}}
